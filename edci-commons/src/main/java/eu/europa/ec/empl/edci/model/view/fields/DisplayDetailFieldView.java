package eu.europa.ec.empl.edci.model.view.fields;

public class DisplayDetailFieldView {
    private MediaObjectFieldView image;
    private Integer page;

    public MediaObjectFieldView getImage() {
        return image;
    }

    public void setImage(MediaObjectFieldView image) {
        this.image = image;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
