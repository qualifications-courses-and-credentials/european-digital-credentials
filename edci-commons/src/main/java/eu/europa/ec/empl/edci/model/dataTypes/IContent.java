package eu.europa.ec.empl.edci.model.dataTypes;

public interface IContent {

    public String getContent();

    public void setContent(String content);

    public String getLanguage();

    public void setLanguage(String lang);

    public String getFormat();

}
