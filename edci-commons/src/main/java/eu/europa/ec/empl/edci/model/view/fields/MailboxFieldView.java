package eu.europa.ec.empl.edci.model.view.fields;

public class MailboxFieldView {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
