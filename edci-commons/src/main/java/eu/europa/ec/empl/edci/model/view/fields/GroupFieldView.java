package eu.europa.ec.empl.edci.model.view.fields;

public class GroupFieldView {
    private String altLabel;
    private String prefLabel;

    public String getAltLabel() {
        return altLabel;
    }

    public void setAltLabel(String altLabel) {
        this.altLabel = altLabel;
    }

    public String getPrefLabel() {
        return prefLabel;
    }

    public void setPrefLabel(String prefLabel) {
        this.prefLabel = prefLabel;
    }

}
