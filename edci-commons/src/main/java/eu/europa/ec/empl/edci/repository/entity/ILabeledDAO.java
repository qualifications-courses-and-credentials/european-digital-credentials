package eu.europa.ec.empl.edci.repository.entity;

public interface ILabeledDAO {

    String getLabel();

    void setLabel(String label);
}
