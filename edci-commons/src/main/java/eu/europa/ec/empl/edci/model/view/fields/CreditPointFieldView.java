package eu.europa.ec.empl.edci.model.view.fields;

public class CreditPointFieldView {

    private String framework;
    private String point;

    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
