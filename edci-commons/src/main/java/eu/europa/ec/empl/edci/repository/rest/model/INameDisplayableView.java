package eu.europa.ec.empl.edci.repository.rest.model;

public interface INameDisplayableView {

    String getDisplayName();

    void setDisplayName(String DisplayName);
}
