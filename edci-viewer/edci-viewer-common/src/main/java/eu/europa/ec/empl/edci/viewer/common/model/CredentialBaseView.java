package eu.europa.ec.empl.edci.viewer.common.model;

public class CredentialBaseView {
    private String uuid;

    public CredentialBaseView() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
