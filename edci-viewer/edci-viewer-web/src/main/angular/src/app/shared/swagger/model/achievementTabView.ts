/**
 * API
 * API Swagger description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { AchievementSpecTabView } from './achievementSpecTabView';
import { ActivityTabView } from './activityTabView';
import { AssessmentTabView } from './assessmentTabView';
import { AwardingProcessFieldView } from './awardingProcessFieldView';
import { CreditPointFieldView } from './creditPointFieldView';
import { EntitlementTabView } from './entitlementTabView';
import { IdentifierFieldView } from './identifierFieldView';
import { LearningOpportunityFieldView } from './learningOpportunityFieldView';
import { LinkFieldView } from './linkFieldView';
import { NoteFieldView } from './noteFieldView';


export interface AchievementTabView { 
    awardedBy?: AwardingProcessFieldView;
    description?: Array<string>;
    identifier?: Array<IdentifierFieldView>;
    additionalNote?: Array<NoteFieldView>;
    supplementaryDocument?: Array<LinkFieldView>;
    title?: string;
    dcType?: Array<string>;
    id?: string;
    provenBy?: Array<AssessmentTabView>;
    influencedBy?: Array<ActivityTabView>;
    entitlesTo?: Array<EntitlementTabView>;
    subAchievements?: Array<AchievementTabView>;
    isPartOf?: Array<AchievementTabView>;
    learningOpportunity?: LearningOpportunityFieldView;
    specifiedBy?: AchievementSpecTabView;
    receivedCredit?: Array<CreditPointFieldView>;
}
