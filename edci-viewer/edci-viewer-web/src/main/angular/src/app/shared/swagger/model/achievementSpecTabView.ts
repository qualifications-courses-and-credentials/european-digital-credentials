/**
 * API
 * API Swagger description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { AwardingOpportunityFieldView } from './awardingOpportunityFieldView';
import { IdentifierFieldView } from './identifierFieldView';
import { LearningOutcomeFieldView } from './learningOutcomeFieldView';
import { LinkFieldView } from './linkFieldView';
import { NoteFieldView } from './noteFieldView';
import { QualificationFieldView } from './qualificationFieldView';


export interface AchievementSpecTabView { 
    altLabel?: Array<string>;
    category?: Array<string>;
    description?: Array<string>;
    homepage?: Array<LinkFieldView>;
    identifier?: Array<IdentifierFieldView>;
    dateModified?: string;
    additionalNote?: Array<NoteFieldView>;
    supplementaryDocument?: Array<LinkFieldView>;
    title?: string;
    dcType?: Array<string>;
    learningOutcome?: Array<LearningOutcomeFieldView>;
    definition?: string;
    learningOutcomeSummary?: NoteFieldView;
    workloadInHours?: string;
    maximumDuration?: string;
    creditPoint?: Array<string>;
    thematicArea?: Array<string>;
    targetGroup?: Array<string>;
    educationLevel?: Array<string>;
    learningSetting?: string;
    entryRequirement?: NoteFieldView;
    educationSubject?: Array<string>;
    language?: Array<string>;
    mode?: Array<string>;
    volumeOfLearning?: string;
    awardingOpportunity?: Array<AwardingOpportunityFieldView>;
    qualification?: QualificationFieldView;
}
