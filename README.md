# EDCI - European Digital Credentials for Learning Infrastructure
[![license: EUPL](licence-EUPL%201.2-brightgreen.svg)](https://github.com/teamdigitale/licenses/blob/master/EUPL-1.2)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

The European Digital Credential for Learning Infrastructure is a set of standards, services and software which allows institutions to issue digital, tamper-evident learning credentials such as diplomas, micro-credentials, certificates of participation etc within the European Education Area. With it learners, employers, education and training providers and other organisations have a simple and trustworthy way of verifying the validity and authenticity of digital credentials. European Digital Credentials for Learning are an application profile of the [European Learning Model](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/snb)-.

## General Information

- [Custom branding in frontend applications](configuration/documentation/branding.md)
- [Managing languages in EDCI applications](configuration/documentation/managing_languages.md)

## Components

### Wallet

The EDCI wallet is a backend api that receives credentials from the issuer, stores them and serves them to wallet front-ends such as Europass or the EDCI Viewer. It also serves 'share' requests to other systems.

More information: [Wallet](edci-wallet/README.md)

### Issuer

The [EDCI Issuer](https://europa.eu/europass/digital-credentials/issuer/#/home) is a web-app provided for any institution that does not wish to develop/install/purchase their own issuing software. It implements the EDCI Credential Standard. The EDCI Issuer is  capable of taking structured data about awarded credentials, and issuing thousands of compliant digitally-signed credentials.

More information: [Issuer](edci-issuer/README.md)

### Viewer

The [EDCI Viewer](https://europa.eu/europass/digital-credentials/viewer/#/home) is used to view a European Digital Credential for Learning received by e-mail or uploaded to a wallet. The credential contains rich data about your accomplishments. The tool will allow you to check the authenticity and validity of the credential, as well as download it as a pdf or create a sharelink.

More information: [Viewer](edci-viewer/README.md)