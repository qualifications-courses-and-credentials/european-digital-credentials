CREATE TABLE COUNTRIES_TEMP
   (	"URL" VARCHAR2(100 CHAR), 
	"CODE" VARCHAR2(100 CHAR), 
	"NAME" VARCHAR2(100 CHAR)
   );

insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GRL', 'GL' , 'Greenland');

insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BDI','BI','Burundi');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BEN','BJ','Benin');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CAF','CF','Central African Republic');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BHR','BH','Bahrain');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KWT','KW','Kuwait');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/OMN','OM','Oman');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/QAT','QA','Qatar');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AFG','AF','Afghanistan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TUV','TV','Tuvalu');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/1A0','1A','Kosovo');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/WSM','WS','Samoa');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/COD','CD','Democratic Republic of the Congo');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SLB','SB','Solomon Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BEL','BE','Belgium');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DEU','DE','Germany');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DNK','DK','Denmark');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FRA','FR','France');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GBR','UK','United Kingdom');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GRC','GR','Greece');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HRV','HR','Croatia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HUN','HU','Hungary');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ITA','IT','Italy');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LTU','LT','Lithuania');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MEX','MX','Mexico');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NLD','NL','Netherlands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/POL','PL','Poland');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PRT','PT','Portugal');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SWE','SE','Sweden');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AUT','AT','Austria');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BGR','BG','Bulgaria');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CYP','CY','Cyprus');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ESP','ES','Spain');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IRL','IE','Ireland');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MAR','MA','Morocco');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MLT','MT','Malta');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MYS','MY','Malaysia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CZE','CZ','Czechia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/EST','EE','Estonia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FIN','FI','Finland');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LVA','LV','Latvia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SVK','SK','Slovakia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SVN','SI','Slovenia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CHE','CH','Switzerland');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LUX','LU','Luxembourg');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ROU','RO','Romania');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VNM','VN','Vietnam');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SEN','SN','Senegal');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SYC','SC','Seychelles');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ZAF','ZA','South Africa');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KOR','KR','South Korea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LBN','LB','Lebanon');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SAU','SA','Saudi Arabia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/JOR','JO','Jordan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/COM','KM','Comoros');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PAK','PK','Pakistan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TUN','TN','Tunisia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MKD','MK','North Macedonia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GNQ','GQ','Equatorial Guinea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AUS','AU','Australia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GIN','GN','Guinea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DZA','DZ','Algeria');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CYM','KY','Cayman Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TKM','TM','Turkmenistan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IRN','IR','Iran');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ARG','AR','Argentina');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/EGY','EG','Egypt');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IDN','ID','Indonesia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CHL','CL','Chile');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BGD','BD','Bangladesh');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/URY','UY','Uruguay');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BMU','BM','Bermuda');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MUS','MU','Mauritius');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BLZ','BZ','Belize');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BRB','BB','Barbados');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CIV','CI','Côte d’Ivoire');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/COG','CG','Congo');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GUY','GY','Guyana');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/JAM','JM','Jamaica');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KEN','KE','Kenya');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MDG','MG','Madagascar');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MWI','MW','Malawi');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SUR','SR','Suriname');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SWZ','SZ','Eswatini');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TTO','TT','Trinidad and Tobago');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TZA','TZ','Tanzania');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/UGA','UG','Uganda');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ZMB','ZM','Zambia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ZWE','ZW','Zimbabwe');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KGZ','KG','Kyrgyzstan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MRT','MR','Mauritania');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SLE','SL','Sierra Leone');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GMB','GM','The Gambia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LBY','LY','Libya');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PAN','PA','Panama');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AND','AD','Andorra');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LIE','LI','Liechtenstein');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NOR','NO','Norway');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/RUS','RU','Russia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SMR','SM','San Marino');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/STP','ST','São Tomé and Príncipe');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TUR','TR','Türkiye');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ISL','IS','Iceland');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GNB','GW','Guinea-Bissau');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CHN','CN','China');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NPL','NP','Nepal');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/USA','US','United States');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ISR','IL','Israel');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/JPN','JP','Japan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CAN','CA','Canada');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IND','IN','India');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MDA','MD','Moldova');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KNA','KN','Saint Kitts and Nevis');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MCO','MC','Monaco');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TCD','TD','Chad');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VUT','VU','Vanuatu');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LAO','LA','Laos');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MLI','ML','Mali');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TGO','TG','Togo');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BRN','BN','Brunei');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/COL','CO','Colombia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PNG','PG','Papua New Guinea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GIB','GI','Gibraltar');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/THA','TH','Thailand');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BRA','BR','Brazil');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NZL','NZ','New Zealand');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CPV','CV','Cabo Verde');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NER','NE','Niger');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ABW','AW','Aruba');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/UZB','UZ','Uzbekistan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MOZ','MZ','Mozambique');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TWN','TW','Taiwan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GTM','GT','Guatemala');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BLR','BY','Belarus');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BIH','BA','Bosnia and Herzegovina');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LKA','LK','Sri Lanka');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FSM','FM','Micronesia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BHS','BS','Bahamas');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ARM','AM','Armenia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DMA','DM','Dominica');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ALB','AL','Albania');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BFA','BF','Burkina Faso');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KAZ','KZ','Kazakhstan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/UKR','UA','Ukraine');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SGP','SG','Singapore');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CMR','CM','Cameroon');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AGO','AO','Angola');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ARE','AE','United Arab Emirates');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GEO','GE','Georgia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DOM','DO','Dominican Republic');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ATG','AG','Antigua and Barbuda');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BOL','BO','Bolivia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CRI','CR','Costa Rica');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CUB','CU','Cuba');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ECU','EC','Ecuador');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GRD','GD','Grenada');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HTI','HT','Haiti');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LCA','LC','Saint Lucia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NIC','NI','Nicaragua');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PER','PE','Peru');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PRY','PY','Paraguay');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SLV','SV','El Salvador');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VCT','VC','Saint Vincent and the Grenadines');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VEN','VE','Venezuela');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SOM','SO','Somalia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FJI','FJ','Fiji');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HKG','HK','Hong Kong');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PLW','PW','Palau');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MNG','MN','Mongolia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SRB','RS','Serbia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MNE','ME','Montenegro');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SYR','SY','Syria');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GAB','GA','Gabon');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HND','HN','Honduras');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LBR','LR','Liberia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PHL','PH','Philippines');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TJK','TJ','Tajikistan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MDV','MV','Maldives');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MAC','MO','Macau');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/RWA','RW','Rwanda');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AZE','AZ','Azerbaijan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KIR','KI','Kiribati');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SDN','SD','Sudan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GHA','GH','Ghana');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BWA','BW','Botswana');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/LSO','LS','Lesotho');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NAM','NA','Namibia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VAT','VA','Vatican City State');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IRQ','IQ','Iraq');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NGA','NG','Nigeria');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FRO','FO','Faroes');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/DJI','DJ','Djibouti');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/COK','CK','Cook Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/YEM','YE','Yemen');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/KHM','KH','Cambodia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TLS','TL','Timor-Leste');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TON','TO','Tonga');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PSE','PS','Palestine');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MHL','MH','Marshall Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/AIA','AI','Anguilla');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ALA','AX','Åland Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ASM','AS','American Samoa');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ATA','AQ','Antarctica');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BES','BQ','Bonaire, Sint Eustatius and Saba');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BLM','BL','Saint Barthélemy');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BTN','BT','Bhutan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/BVT','BV','Bouvet Island');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CCK','CC','Cocos (Keeling) Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CPT','CP','Clipperton');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CUW','CW','Curaçao');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/CXR','CX','Christmas Island');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ERI','ER','Eritrea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ESH','EH','Western Sahara');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/ETH','ET','Ethiopia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FLK','FK','Falkland Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/FQ0','TF','French Southern and Antarctic Lands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GGY','GG','Guernsey');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GLP','GP','Guadeloupe');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GUF','GF','French Guiana');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/GUM','GU','Guam');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/HMD','HM','Heard Island and McDonald Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IMN','IM','Isle of Man');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/IOT','IO','British Indian Ocean Territory');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/JEY','JE','Jersey');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MAF','MF','Saint Martin');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MMR','MM','Myanmar/Burma');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MNP','MP','Northern Mariana Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MSR','MS','Montserrat');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MTQ','MQ','Martinique');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/MYT','YT','Mayotte');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NCL','NC','New Caledonia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NFK','NF','Norfolk Island');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NIU','NU','Niue');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/NRU','NR','Nauru');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PCN','PN','Pitcairn Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PRI','PR','Puerto Rico');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PRK','KP','North Korea');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/PYF','PF','French Polynesia');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/REU','RE','Réunion');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SGS','GS','South Georgia and the South Sandwich Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SHN','SH','Saint Helena, Ascension and Tristan da Cunha');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SJM','SJ','Svalbard and Jan Mayen');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SPM','PM','Saint Pierre and Miquelon');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SSD','SS','South Sudan');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/SXM','SX','Sint Maarten');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TCA','TC','Turks and Caicos Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/TKL','TK','Tokelau');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VGB','VG','British Virgin Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/VIR','VI','US Virgin Islands');
		
insert into COUNTRIES_TEMP (URL, CODE, NAME) VALUES ('http://publications.europa.eu/resource/authority/country/WLF','WF','Wallis and Futuna');

CREATE TABLE TEMP_DATA (DT_LEGAL_IDENTIFIER_PK, SPATIAL_ID, DT_CODE_PK, DT_TEXT_PK, DT_CONTENT_PK)
  AS (select DT_IDENTIFIER.PK, SPATIAL_ID, DT_CODE_SEQ.nextval, DT_TEXT_SEQ.nextval, DT_CONTENT_SEQ.nextval
from DT_IDENTIFIER);

Insert into dt_Content (PK, CONTENT, FORMAT, LANGUAGE)
    select DT_CONTENT_PK, name, 'text/plain', 'en' from TEMP_DATA join COUNTRIES_TEMP on CODE = SPATIAL_ID;

Insert into dt_text (PK)
    select DT_TEXT_PK from TEMP_DATA join COUNTRIES_TEMP on CODE = SPATIAL_ID;

INSERT INTO FIELD_DT_TEXT_CONTENTS (
    Select TEMP.DT_TEXT_PK, TEMP.DT_CONTENT_PK from TEMP_DATA TEMP join COUNTRIES_TEMP on CODE = SPATIAL_ID
);

Insert into dt_code (PK, TARGET_NAME_PK)
    select DT_CODE_PK, DT_TEXT_PK from TEMP_DATA join COUNTRIES_TEMP on CODE = SPATIAL_ID;

merge into DT_IDENTIFIER
using (select * from TEMP_DATA join COUNTRIES_TEMP on CODE = SPATIAL_ID) TEMP_DATA
on (DT_IDENTIFIER.PK = TEMP_DATA.DT_LEGAL_IDENTIFIER_PK)
when matched then update
set SPATIAL_CODE_ID_PK = TEMP_DATA.DT_CODE_PK;

alter table DT_IDENTIFIER drop column SPATIAL_ID;

DROP TABLE TEMP_DATA;

DROP TABLE COUNTRIES_TEMP;
