CREATE TABLE TEMP_DATA (CREDENTIAL_PK, DESCRIPTION_PK, DESCRIPTION_TEXT_PK)
  AS (SELECT PK, DESCRIPTION_PK, DT_TEXT_SEQ.nextval from SPEC_EUROPASS_CREDENTIAL where DESCRIPTION_PK is not null);

INSERT INTO DT_TEXT (PK)
    Select TEMP.DESCRIPTION_TEXT_PK from TEMP_DATA TEMP;

INSERT INTO FIELD_DT_TEXT_CONTENTS (DT_TEXT_PK, DT_CONT_PK)
    Select TEMP.DESCRIPTION_TEXT_PK, text.dt_cont_pk from FIELD_DT_NOTE_CONTENTS TEXT JOIN TEMP_DATA TEMP ON TEXT.DT_NOTE_PK = TEMP.DESCRIPTION_PK;

DELETE FROM FIELD_DT_NOTE_CONTENTS WHERE DT_NOTE_PK in (select DESCRIPTION_PK from TEMP_DATA);

merge into SPEC_EUROPASS_CREDENTIAL
using TEMP_DATA
on (SPEC_EUROPASS_CREDENTIAL.PK = TEMP_DATA.CREDENTIAL_PK)
when matched then update
set DESCRIPTION_TEXT_PK = TEMP_DATA.DESCRIPTION_TEXT_PK;

alter table SPEC_EUROPASS_CREDENTIAL drop column DESCRIPTION_PK;

DROP TABLE TEMP_DATA;