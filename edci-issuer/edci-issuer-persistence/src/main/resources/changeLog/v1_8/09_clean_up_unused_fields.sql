DROP TABLE FIELD_DC_LEA_SPE_IDENTIFIER;
DROP TABLE FIELD_DC_LEA_SPE_ALT_LABEL;
drop table FIELD_DC_LEA_SPE_CRED_POIN;
alter table DC_LEARNING_SPECIFICATION drop column ECTS_CRED_POINTS_PK;
alter table DC_LEARNING_SPECIFICATION drop column ACCREDITATION_ID;

DROP TABLE FIELD_DC_LE_ACT_S_ALTERN_LABEL;

alter table DC_LOCATION drop column GEOGRAPHIC_NAME_PK;

DROP table DT_WEB_DOCUMENT_ACT_TYPE;

ALTER TABLE DT_AMOUNT DROP COLUMN UNIT;

ALTER TABLE DT_IDENTIFIER DROP COLUMN IDENT_SCHEME_AGENCY_NAME;
ALTER TABLE DT_IDENTIFIER DROP COLUMN IDENTIFIER_TYPE;

ALTER TABLE DT_RESULT_CATEGORY DROP COLUMN LABEL_PK;
ALTER TABLE DT_RESULT_CATEGORY DROP COLUMN SCORE_FK;
ALTER TABLE DT_RESULT_CATEGORY DROP COLUMN MIN_SCORE_FK;
ALTER TABLE DT_RESULT_CATEGORY DROP COLUMN MAX_SCORE_FK;
DROP TABLE FIELD_DT_SHO_GRAD_CATEGORY;

ALTER TABLE DT_RESULT_DISTRIB DROP COLUMN DESCRIPTION_PK;

ALTER TABLE DT_SHORT_GRADING DROP COLUMN PERCENTAGE_LOWER_PK;
ALTER TABLE DT_SHORT_GRADING DROP COLUMN PERCENTAGE_EQUAL_PK;
ALTER TABLE DT_SHORT_GRADING DROP COLUMN PERCENTAGE_HIGHER_PK;

ALTER TABLE SPEC_ASSESSMENT DROP COLUMN GRADE_PK;

ALTER TABLE SPEC_ORGANIZATION DROP COLUMN ACCREDITATION_ID;

DROP TABLE FIELD_DC_ENTITL_S_LIMIT_N_OCC;
DROP TABLE FIELD_DC_LEA_SPE_EDU_LVL;
DROP TABLE FIELD_DC_LEA_SPE_QUAL_CODE;
DROP TABLE FIELD_DC_LEA_SPE_EDU_SUBJ;

DROP TABLE FIELD_DT_STAND_IDENTIFIER;
DROP TABLE FIELD_DT_STAND_SUPPLEM_DOCUMENT;
DROP TABLE DT_STANDARD;