/**
 * API
 * API Swagger description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface CustomizableInstanceFieldView { 
    frontId?: string;
    position?: number;
    label?: string;
    fieldPath?: string;
    fieldType?: CustomizableInstanceFieldView.FieldTypeEnum;
    mandatory?: boolean;
    toolTip?: string;
    validation?: string;
    controlledListType?: string;
    additionalInfo?: Array<string>;
}
export namespace CustomizableInstanceFieldView {
    export type FieldTypeEnum = 'TEXT' | 'TEXT_AREA' | 'DATE' | 'CONTROLLED_LIST';
    export const FieldTypeEnum = {
        TEXT: 'TEXT' as FieldTypeEnum,
        TEXTAREA: 'TEXT_AREA' as FieldTypeEnum,
        DATE: 'DATE' as FieldTypeEnum,
        CONTROLLEDLIST: 'CONTROLLED_LIST' as FieldTypeEnum
    };
}
