/**
 * API
 * API Swagger description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { AdditionalInfo } from './additionalInfo';
import { CodeDTView } from './codeDTView';
import { ContactPointDCView } from './contactPointDCView';
import { GroupDCView } from './groupDCView';
import { IdentifierDTView } from './identifierDTView';
import { LegalIdentifierDTView } from './legalIdentifierDTView';
import { LocationDCView } from './locationDCView';
import { MediaObjectDTView } from './mediaObjectDTView';
import { NoteDTView } from './noteDTView';
import { SubresourcesOids } from './subresourcesOids';
import { TextDTView } from './textDTView';
import { WebDocumentDCView } from './webDocumentDCView';


export interface OrganizationSpecView { 
    oid?: number;
    additionalInfo?: AdditionalInfo;
    displayName?: string;
    label?: string;
    defaultLanguage: string;
    legalName?: TextDTView;
    vatIdentifier?: Array<LegalIdentifierDTView>;
    taxIdentifier?: Array<LegalIdentifierDTView>;
    homePage?: Array<WebDocumentDCView>;
    logo?: MediaObjectDTView;
    identifier?: Array<IdentifierDTView>;
    registration?: LegalIdentifierDTView;
    dcType?: Array<CodeDTView>;
    modified?: string;
    eidasIdentifier?: LegalIdentifierDTView;
    altLabel?: TextDTView;
    additionalNote?: Array<NoteDTView>;
    relSubOrganizationOf?: SubresourcesOids;
    relAccreditation?: SubresourcesOids;
    location: Array<LocationDCView>;
    groupMemberOf?: Array<GroupDCView>;
    contactPoint?: Array<ContactPointDCView>;
}
