export interface StepInterface {
    labelKey: string;
    isCompleted: boolean;
    isActive: boolean;
    isInvalid: boolean;
    isDisabled: boolean;
    index: number;
}
