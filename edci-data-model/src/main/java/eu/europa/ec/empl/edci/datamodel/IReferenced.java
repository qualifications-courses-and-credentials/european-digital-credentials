package eu.europa.ec.empl.edci.datamodel;

public interface IReferenced {

    Object getReferenced();
}
