package eu.europa.ec.empl.edci.model.external;

import java.net.URI;

public class ConceptSchemeReport {
    private URI id;

    public URI getId() {
        return id;
    }

    public void setId(URI id) {
        this.id = id;
    }
}
