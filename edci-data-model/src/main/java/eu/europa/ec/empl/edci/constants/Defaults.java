package eu.europa.ec.empl.edci.constants;

public class Defaults {

    public static final String LOCALE = "en";
    public static final String IDENTIFIER_PREFIX = "urn:epass:default:";

}
