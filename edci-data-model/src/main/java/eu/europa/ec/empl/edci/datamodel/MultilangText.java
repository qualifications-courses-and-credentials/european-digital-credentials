package eu.europa.ec.empl.edci.datamodel;


public interface MultilangText {

    String getContent();

    String getLanguage();

    String getFormat();

}
